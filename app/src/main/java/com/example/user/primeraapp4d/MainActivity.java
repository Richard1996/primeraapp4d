package com.example.user.primeraapp4d;

import android.Manifest;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Vibrator;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, FragUno.OnFragmentInteractionListener, FragDos.OnFragmentInteractionListener{

    Button btnLogin, buttonBuscar, buttonGuardar, buttonParametro, buttonFragUno, buttonFragDos, boton, botonVibrar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnLogin=(Button)findViewById(R.id.buttonLogin);
        buttonBuscar=(Button)findViewById(R.id.buttonBuscar);
        buttonGuardar=(Button)findViewById(R.id.buttonGuardar);
        buttonParametro=(Button)findViewById(R.id.buttonParametro);
        buttonFragUno = (Button)findViewById(R.id.btnFrgUno) ;
        buttonFragDos = (Button)findViewById(R.id.btnFrgDos);
        buttonFragDos.setOnClickListener(this);
        buttonFragUno.setOnClickListener(this);

        buttonParametro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, PasarParametro.class);
                startActivity(intent);
            }
        });

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, ActividadLogin.class);
                startActivity(intent);
            }
        });

        buttonBuscar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, ActividadBuscar.class);
                startActivity(intent);
            }
        });

        buttonGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, ActividadRegistrar.class);
                startActivity(intent);
            }
        });

    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent;
        switch (item.getItemId()){
            case R.id.opcionLogin:
                //traspaso de actividades
                intent=new Intent(MainActivity.this,ActividadLogin.class);
                startActivity(intent);
                break;
            case R.id.opcionRegistrar:
                intent=new Intent(MainActivity.this,ActividadRegistrar.class);
                startActivity(intent);
                break;
            case R.id.opcionIngresar:
                //dialogo de textos,generamos un evento al boton dialogo
                Dialog dialogoLogin = new Dialog(MainActivity.this);
                dialogoLogin.setContentView(R.layout.dlg_login);
                Button botonAutenticar = (Button) dialogoLogin.findViewById(R.id.buttonIngresar);
                final EditText cajaUsuario = (EditText)dialogoLogin.findViewById(R.id.editText);
                final EditText cajaClave = (EditText)dialogoLogin.findViewById(R.id.editText2);
                //generamos evento anonimo al boto autenticar
                botonAutenticar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Toast.makeText(MainActivity.this, cajaUsuario.getText().toString() + " " + cajaClave.getText().toString(),Toast.LENGTH_SHORT);
                        }
                });
                dialogoLogin.show();
                break;
            case R.id.opcionGuardar:
                //dialogo de texto
                Dialog dialogoRegistrar = new Dialog(MainActivity.this);
                dialogoRegistrar.setContentView(R.layout.dlg_registrar);
                Button botonEnviar = (Button) dialogoRegistrar.findViewById(R.id.buttonGuardar);
                final TextView Nombre = (TextView)dialogoRegistrar.findViewById(R.id.textView2);
                final EditText cajaNombres = (EditText)dialogoRegistrar.findViewById(R.id.editText3);
                final TextView Apellidos = (TextView)dialogoRegistrar.findViewById(R.id.textView3);
                final EditText cajaApellidos = (EditText)dialogoRegistrar.findViewById(R.id.editText4);
                final TextView ingreseDatos =(TextView) dialogoRegistrar.findViewById(R.id.textView4);
                botonEnviar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Toast.makeText(MainActivity.this, ingreseDatos.getText().toString() +" " + Nombre .getText().toString() + " " + cajaNombres.getText().toString() + " " + Apellidos.getText().toString() + " " + cajaApellidos.getText().toString(),Toast.LENGTH_SHORT);
                    }
                });
                dialogoRegistrar.show();
                break;
            case R.id.opcionVibracion:
                intent=new Intent(MainActivity.this,ActividadVibrar.class);
                startActivity(intent);
                break;
            case R.id.opcionAcelerometro:
                intent=new Intent(MainActivity.this,ActividadAcelerometro.class);
                startActivity(intent);
                break;
            case R.id.opcionProximidad:
                intent=new Intent(MainActivity.this,ActividadProximidad.class);
                startActivity(intent);
                break;

        }
        return true;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btnFrgUno:
                FragUno fragmentoUno = new FragUno();
                FragmentTransaction transactionUno = getSupportFragmentManager().beginTransaction();
                transactionUno.replace(R.id.contenedor,fragmentoUno);
                transactionUno.commit();
                break;
            case R.id.btnFrgDos:
                FragDos fragmentoDos = new FragDos();
                FragmentTransaction transactionDos = getSupportFragmentManager().beginTransaction();
                transactionDos.replace(R.id.contenedor,fragmentoDos);
                transactionDos.commit();
                break;
        }
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }
}
